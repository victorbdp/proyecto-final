# README #

### topologia###
### comando de cisco ###
Switch>enable 
Switch#conf ter
Enter configuration commands, one per line. End with CNTL/Z.
Switch(config)#vlan 10
Switch(config-vlan)#NAME VLAN10
Switch(config-vlan)#EXIT
Switch(config)#VLAN 20
Switch(config-vlan)#NAME VLAN20
Switch(config-vlan)#EXIT
Switch(config)#VLAN 30
Switch(config-vlan)#NAME VLAN30
Switch(config-vlan)#EXIT


### DAR UN RANGO DE PUERTOS A LAS VLAN ###

Switch(config-if-range)#interface range f0/9-14
Switch(config-if-range)#sw
Switch(config-if-range)#switchport mode access
Switch(config-if-range)#sw
Switch(config-if-range)#switchport access vlan 10
Switch(config-if-range)#exit
Switch(config)#interface range f0/15-19
Switch(config-if-range)#switchport mode access
Switch(config-if-range)#switchport access vlan 20
Switch(config-if-range)#exit
Switch(config)#interface range f0/20-24
Switch(config-if-range)#switchport mode access
Switch(config-if-range)#switchport access vlan 30
Switch(config-if-range)#exit
Switch(config)#exit

### CONFIGURACION DEL SWITCH Y TAMBIEN ASIGNAR NOMBRES A LA VLAN###

Switch#conf ter
Enter configuration commands, one per line. End with CNTL/Z.
Switch(config)#hostname S1
S1(config)#no ip domain-lookup
S1(config)#service password-encryption
S1(config)#enable secret class
S1(config)#banner motd #ACCESO NO AUTORIZADO SALESIANA#
S1(config)#enable password cisco 
S1(config)#line console 0
S1(config-line)#password cisco
S1(config-line)#login
S1(config-line)#logging s
S1(config-line)#logging synchronous 
S1(config-line)#line vty 0 15
S1(config-line)#password cisco
S1(config-line)#logging s
S1(config-line)#logging synchronous 
S1(config-line)#login
S1(config-line)#exit
S1(config)#vlan 10
S1(config-vlan)#name UBUNTU
S1(config-vlan)#vlan 20
S1(config-vlan)#name MINT
S1(config-vlan)#vlan 30
S1(config-vlan)#name DEBIAN
S1(config-vlan)#end
S1#





### CREACION VLAN EN LOS SWITCH ###
S1(config) #Vlan 10
Si (config-vlan)# name estudiantes

S2(config-vlan)#Vlan 20
S2(config-vlan)# name Docentes
S2(config-vlan)# Vlan 99
S2(config-vlan)# name Dirigentes
S2(config-vlan)#end


ping 192.168.1.1
Swithch 1	192.168.1.153	192.168.1.1
Linux_Mint	192.168.2.20	192.168.1.1
Ubuntu	   192.168.1.10	        192.168.1.1
Debian	   192.168.1.2	        192.168.1.1

